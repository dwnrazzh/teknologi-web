

<body>
    <div class="container">
        <div class="row text-center ">
            <div class="col-md-12">
                <br /><br />
                <h2> Login Admin</h2>
               
                <h5>( Login yourself to get access )</h5>
            </div>
            <?= $this->session->flashdata('message'); ?>
        </div>
         <div class="row ">
               
                  <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                        <strong>   Enter Details To Login </strong>  
                            </div>
                            <div class="panel-body">
                                <form class="user" method="post" action="<?=base_url('auth'); ?>">
                                       <br />
                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text" class="form-control form-control-user" id="email" name="email" placeholder="Your Username" value="<?= set_value('email'); ?>"/>
                                            <?= form_error('email', '<small class="text-danger pl-3">','</small>'); ?>
                                        </div>
                                    <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" class="form-control form-control-user" id="password" name="password"  placeholder="Your Password" />
                                            <?= form_error('password', '<small class="text-danger pl-3">','</small>'); ?>
                                        </div>
                                     
                                     <button type="submit" class="btn btn-primary ">Login Now</button>
                                    <hr />
                                    Not register ? <a href="<?= base_url('auth/registration'); ?>" >click here </a> 
                                    </form>
                            </div>
                           
                        </div>
                    </div>
                
                
        </div>
    </div>


    